#!/usr/bin/env cwl-runner

# --------------------------------------------------- COMMAND LINE TOOL INFO ----------------------------------------------------

cwlVersion: v1.0
class: CommandLineTool

# -------------------------------------------------------- BASE COMMAND ---------------------------------------------------------

baseCommand: ['python3', '/train.py']

# ----------------------------------------------------- HINTS/REQUIREMENTS ------------------------------------------------------
$namespaces:
  cwltool: http://commonwl.org/cwltool#
requirements:
  cwltool:MPIRequirement:
    processes: $(inputs.workers)
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc5/sc5_workflow_rltrain:latest
  ResourceRequirement:
    ramMin: 16000
    outdirMin: 1000
# --------------------------------------------------------- TOOL INPUTS ---------------------------------------------------------

inputs:
  env:
    type: string
    inputBinding:
      position: 1
  pcon:
    type: string?
    inputBinding:
      position: 2
      prefix: --pcon
  rcon:
    type: string?
    inputBinding:
      position: 3
      prefix: --rcon
  workers:
    type: int?
    inputBinding:
      position: 4
      prefix: --workers
  iterations:
    type: int?
    inputBinding:
      position: 5
      prefix: --iterations
  horizon:
    type: int?
    inputBinding:
      position: 6
      prefix: --horizon

# -------------------------------------------------------- TOOL OUTPUTS ---------------------------------------------------------

stdout: output_file.txt
outputs:
  out:
    type: stdout
  out_data:
    type: File
    outputBinding:
      glob: storage/experiments/*/progress.json
