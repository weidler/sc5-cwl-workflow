import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import pprint
import traceback

import distance

import numpy as np

with np.testing.suppress_warnings() as sup:
    sup.filter(DeprecationWarning)
    sup.filter(UserWarning)

    from angorapy.train import run_experiment
    from angorapy.utilities.defaults import autoselect_distribution

    import tensorflow as tf

    import argparse
    import logging

    import argcomplete
    from gym.spaces import Box, Discrete, MultiDiscrete

    from angorapy.configs import hp_config
    from angorapy.common.policies import get_distribution_by_short_name
    from angorapy.models import get_model_builder
    from angorapy.common.const import COLORS
    from angorapy.utilities.monitoring import Monitor
    from angorapy.utilities.util import env_extract_dims
    from angorapy.common.wrappers import make_env
    from angorapy.common.transformers import StateNormalizationTransformer, RewardNormalizationTransformer
    from angorapy.agent.ppo_agent import PPOAgent

    from angorapy.environments import *

    from mpi4py import MPI


    if __name__ == "__main__":

        tf.get_logger().setLevel('INFO')
        all_envs = [e.id for e in list(gym.envs.registry.all())]

        # parse commandline arguments
        parser = argparse.ArgumentParser(description="Train a PPO Agent on some task.")

        # general parameters
        parser.add_argument("env", nargs='?', type=str, default="LunarLander-v2", help="the target gym environment")
        parser.add_argument("--architecture", choices=["simple", "deeper", "wider", "shadow"], default="simple",
                            help="architecture of the policy")
        parser.add_argument("--model", choices=["ffn", "rnn", "lstm", "gru"], default="ffn",
                            help=f"model type if architecture allows for choices")
        parser.add_argument("--distribution", type=str, default=None,
                            choices=["categorical", "gaussian", "beta", "multi-categorical"])
        parser.add_argument("--shared", action="store_true",
                            help=f"make the model share part of the network for policy and value")
        parser.add_argument("--iterations", type=int, default=5000, help=f"number of iterations before training ends")

        # meta arguments
        parser.add_argument("--pcon", type=str, default=None, help="config name (utilities/hp_config.py) to be loaded")
        parser.add_argument("--rcon", type=str, default=None,
                            help="config (utilities/reward_config.py) of the reward function")
        parser.add_argument("--cpu", action="store_true", help=f"use cpu only")
        parser.add_argument("--sequential", action="store_true", help=f"run worker sequentially workers")
        parser.add_argument("--load-from", type=int, default=None, help=f"load from given agent id")
        parser.add_argument("--preload", type=str, default=None, help=f"load visual component weights from pretraining")
        parser.add_argument("--export-file", type=int, default=None, help=f"save policy to be loaded in workers into file")
        parser.add_argument("--eval", action="store_true", help=f"evaluate additionally to have at least 5 eps")
        parser.add_argument("--radical-evaluation", action="store_true", help=f"only record stats from seperate evaluation")
        parser.add_argument("--save-every", type=int, default=0, help=f"save agent every given number of iterations")
        parser.add_argument("--monitor-frequency", type=int, default=1, help=f"update the monitor every n iterations.")
        parser.add_argument("--gif-every", type=int, default=0, help=f"make a gif every n iterations.")
        parser.add_argument("--debug", action="store_true", help=f"run in debug mode (eager mode)")
        parser.add_argument("--no-monitor", action="store_true", help="dont use a monitor")

        # gathering parameters
        parser.add_argument("--workers", type=int, default=8, help=f"the number of workers exploring the environment")
        parser.add_argument("--horizon", type=int, default=2048,
                            help=f"number of time steps one worker generates per cycle")
        parser.add_argument("--discount", type=float, default=0.99, help=f"discount factor for future rewards")
        parser.add_argument("--lam", type=float, default=0.97, help=f"lambda parameter in the GAE algorithm")
        parser.add_argument("--no-state-norming", action="store_true", help=f"do not normalize states")
        parser.add_argument("--no-reward-norming", action="store_true", help=f"do not normalize rewards")

        # optimization parameters
        parser.add_argument("--epochs", type=int, default=3, help=f"the number of optimization epochs in each cycle")
        parser.add_argument("--batch-size", type=int, default=64, help=f"minibatch size during optimization")
        parser.add_argument("--lr-pi", type=float, default=1e-3, help=f"learning rate of the policy")
        parser.add_argument("--lr-schedule", type=str, default=None, choices=[None, "exponential"],
                            help=f"lr schedule type")
        parser.add_argument("--clip", type=float, default=0.2, help=f"clipping range around 1 for the objective function")
        parser.add_argument("--c-entropy", type=float, default=0.01, help=f"entropy factor in objective function")
        parser.add_argument("--c-value", type=float, default=1, help=f"value factor in objective function")
        parser.add_argument("--tbptt", type=int, default=16, help=f"length of subsequences in truncated BPTT")
        parser.add_argument("--grad-norm", type=float, default=0.5, help=f"norm for gradient clipping, 0 deactivates")
        parser.add_argument("--clip-values", action="store_true", help=f"clip value objective")
        parser.add_argument("--stop-early", action="store_true", help=f"stop early if threshold of env was surpassed")
        parser.add_argument("--experiment-group", type=str, default="default", help="experiment group identifier")

        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        is_root = rank == 0

        # read arguments
        argcomplete.autocomplete(parser)
        args = parser.parse_args()

        if args.env not in all_envs:
            if is_root:
                indices = np.argsort([distance.levenshtein(w, args.env) for w in all_envs])[:3]
                print(f"Unknown environment {args.env}. Did you mean one of {[all_envs[i] for i in indices]}")
            exit()

        # if config is given load it as default, then overwrite with any goal given parameters
        if args.pcon is not None:
            try:
                parser.set_defaults(**getattr(hp_config, args.pcon))
                args = parser.parse_args()
                if is_root:
                    print(f"Loaded Config {args.pcon}.")
            except AttributeError as err:
                raise ImportError("Cannot find config under given name. Does it exist in utilities/hp_config.py?")

        if args.debug:
            tf.config.run_functions_eagerly(True)
            if is_root:
                logging.warning("YOU ARE RUNNING IN DEBUG MODE!")

        try:
            run_experiment(args.env, vars(args), use_monitor=not args.no_monitor)
        except Exception as e:
            if rank == 0:
                traceback.print_exc()
