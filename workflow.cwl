# -------------------------------------------------------- WORKFLOW INFO --------------------------------------------------------

cwlVersion: v1.0
class: Workflow

# ------------------------------------------------------- WORKFLOW INPUTS -------------------------------------------------------

inputs:
  env: string
  pcon: string
  rcon: string
  workers: int          # (32-bit signed)
  iterations: int
  horizon: int
  output_file_name: string

# ------------------------------------------------------- WORKFLOW OUTPUTS ------------------------------------------------------

outputs:
  output_file:
    type: File
    outputSource: reinforcement_learning/out
  output_progress:
    type: File
    outputSource: reinforcement_learning/out_data
  output_visualization:
    type: File
    outputSource: visualization/plot

# ------------------------------------------------------- WORKFLOW STEPS --------------------------------------------------------

steps:
  reinforcement_learning:
    run: step1/reinforcement_learning_tool.cwl
    in:
      env: env
      pcon: pcon
      rcon: rcon
      workers: workers
      iterations: iterations
      horizon: horizon
    out: [out, out_data]
  visualization:
    run: step2/analysis_tool.cwl
    in:
      input_file: reinforcement_learning/out_data
      output_file_name: output_file_name
    out: [plot]