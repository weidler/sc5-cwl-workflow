#!/usr/bin/env python3

import argparse
import numpy as np
import matplotlib.pyplot as plt
import json

parser = argparse.ArgumentParser()
parser.add_argument('input_file', help='json file containing the progress indicators')
parser.add_argument('--output_file', default='out.png', help='file where output plot should be written')
args = parser.parse_args()

fig, ax = plt.subplots(figsize=(12, 6))

with open(args.input_file) as f:
    data = json.load(f)

plt.plot(data["rewards"]["mean"])

ax.set_xlabel('cycle')
ax.set_ylabel('reward')
ax.legend()

plt.savefig(args.output_file)
