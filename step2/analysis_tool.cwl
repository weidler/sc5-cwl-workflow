cwlVersion: v1.0
class: CommandLineTool

baseCommand: ['python3', '/visualization.py']


hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc5/sc5_workflow_visualization:latest
  ResourceRequirement:
    ramMin: 2048
    outdirMin: 4096

inputs:
  input_file:
    type: File
    inputBinding:
      position: 1
  output_file_name:
    type: string
    inputBinding:
      prefix: --output_file
      position: 2

outputs:
  plot:
    type: File
    outputBinding:
      glob: $(inputs.output_file_name)
